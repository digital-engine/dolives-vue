const draw = (id, width, height) => {
    const canvas = document.getElementById(id);
    // 设置充满全屏
    canvas.width = width;
    canvas.height = height;
    // 清空
    const ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.save();
    // 设置背景
    ctx.fillStyle = '#FFFFFF';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    // 画网格线
    drawGrid(canvas, ctx, 15, 15);
}

const drawGrid = (canvas, ctx, stepX, stepY) => {
    ctx.strokeStyle = '#D4D4D4';
    for (let x = 0; x < canvas.width; x += stepX) {
        // 变粗线
        ctx.lineWidth = x % 60 == 0 ? 0.5 : 0.3;
        ctx.beginPath();
        ctx.moveTo(x, 0);
        ctx.lineTo(x, canvas.height);
        ctx.stroke();
    }
    for (let y = 0; y < canvas.height; y += stepY) {
        // 变粗线
        ctx.lineWidth = y % 60 == 0 ? 0.5 : 0.3;
        ctx.beginPath();
        ctx.moveTo(0, y);
        ctx.lineTo(canvas.width, y);
        ctx.stroke();
    }
    ctx.restore();
}

export default draw
