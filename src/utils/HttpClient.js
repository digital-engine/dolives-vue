import axios from 'axios';
import { ElMessage } from 'element-plus'

const urlPrefix = "http://localhost:80/admin/";

const sendGet = (url, params, func) => {
    axios.get(urlPrefix + url, {
        params: params

    }).then((res) => {
        func(handleRes(res), url, params)

    }).catch((err) => {
        ElMessage.error(err)
    });
}

const sendPost = (url, params, func) => {
    axios.post(urlPrefix + url,
        params

    ).then((res) => {
        func(handleRes(res), url, params)

    }).catch((err) => {
        ElMessage.error(err)
    });
}

const handleRes = (res) => {
    const resData = res.data;
    const code = resData.code;
    const message = resData.message;
    const data = resData.data;
    if (code === 0 || code === 200) {
        return data;
    }
    ElMessage.error(message);
    return undefined;
}

const httpClient = {
    sendGet: sendGet,
    sendPost: sendPost
}

export default httpClient