const config = {
    datePicker: {
        // 大小
        size: 'default',
        // 快捷按钮
        shortcuts: [
            {
                text: '近一周',
                value: () => {
                    const start = new Date()
                    const end = new Date()
                    start.setTime(start.getTime() - 3600 * 1000 * 24 * 7)
                    return [start, end]
                },
            },
            {
                text: '近一个月',
                value: () => {
                    const start = new Date()
                    const end = new Date()
                    start.setTime(start.getTime() - 3600 * 1000 * 24 * 30)
                    return [start, end]
                },
            },
            {
                text: '近三个月',
                value: () => {
                    const start = new Date()
                    const end = new Date()
                    start.setTime(start.getTime() - 3600 * 1000 * 24 * 90)
                    return [start, end]
                },
            },
        ]
    }
}
export default config