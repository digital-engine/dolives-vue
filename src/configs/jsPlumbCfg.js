// 全局配置
const defaultSetting = {
    // 连线样式
    PaintStyle: {
        stroke: '#f76258',
        strokeWidth: 2,
        outlineStroke: 'transparent',
        outlineWidth: 10 // 设定线外边的宽，单位px
    },
    // 鼠标悬停连线样式
    HoverPaintStyle: {
        stroke: 'skyblue',
        strokeWidth: 3,
        cursor: 'pointer'
    },
    // 箭头
    // Overlays: [
    //     ['Arrow', {
    //         width: 12,
    //         length: 10,
    //         location: 1,
    //         direction: 1,
    //         foldback: 0.623
    //     }]
    // ],
}

// 端点样式
const endpointStyle = {
    isSource: true,
    isTarget: true,
    endpoint: "Dot",
    paintStyle: {
        radius: 5,
        fill: '#445566'
    },
    connector: ["Bezier", { curviness: 10 }],
}

// 连接样式
const connectionStyle = {
}

export { defaultSetting, endpointStyle, connectionStyle }