import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
    {
        path: '/',
        component: () => import('../components/scene/Homepage.vue'),
        children: []
    },
    {
        path: '/login',
        component: () => import('../components/scene/Login.vue'),
        children: []
    },
    {
        path: '/homepage',
        component: () => import('../components/scene/Homepage.vue'),
        children: [
            {
                path: '/moduleBody',
                component: () => import('../components/module/body/ModuleBody.vue'),
                children: [
                    {
                        path: '/projectView',
                        component: () => import('../components/module/view/ProjectView.vue')
                    },
                    {
                        path: '/domainView',
                        component: () => import('../components/module/view/DomainView.vue')
                    },
                    {
                        path: '/subdomainView',
                        component: () => import('../components/module/view/SubdomainView.vue')
                    },
                    {
                        path: '/versionView',
                        component: () => import('../components/module/view/VersionView.vue')
                    },
                    {
                        path: '/baseModuleView',
                        component: () => import('../components/module/view/BaseModuleView.vue')
                    },
                    {
                        path: '/bizModuleView',
                        component: () => import('../components/module/view/BizModuleView.vue')
                    },
                    {
                        path: '/launcherModuleView',
                        component: () => import('../components/module/view/LauncherModuleView.vue')
                    },
                    {
                        path: '/configView',
                        component: () => import('../components/module/subview/ConfigView.vue')
                    },
                    {
                        path: '/namingRuleView',
                        component: () => import('../components/module/subview/NamingRuleView.vue')
                    },
                    {
                        path: '/packageStructView',
                        component: () => import('../components/module/view/PackageStructView.vue')
                    },
                    {
                        path: '/generatorView',
                        component: () => import('../components/module/view/GeneratorView.vue')
                    },
                    {
                        path: '/organizationView',
                        component: () => import('../components/module/view/OrganizationView.vue')
                    },
                    {
                        path: '/teamView',
                        component: () => import('../components/module/view/TeamView.vue')
                    },
                    {
                        path: '/memberView',
                        component: () => import('../components/module/view/MemberView.vue')
                    },
                ]
            },
            {
                path: '/modelBody',
                component: () => import('../components/model/body/ModelBody.vue'),
                children: []
            },
            {
                path: '/flowBody',
                component: () => import('../components/flow/FlowBody.vue'),
                children: []
            },
        ]
    },
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router